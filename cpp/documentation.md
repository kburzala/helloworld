# C / C++

[C++](https://en.wikipedia.org/wiki/C%2B%2B) is a high-level general-purpose programming language created by Danish computer scientist `Bjarne Stroustrup` as an extension of the C programming language, or "C with Classes". The language has expanded significantly over time, and modern C++ now has object-oriented, generic, and functional features in addition to facilities for low-level memory manipulation.

---
## Setup

### `C++ Toolset`

    First we will download and install msys2.
    After that we use the series of commands to install packages and update system.

    Commands used :
    Update the package database and base packages using
    pacman -Syu

    Update rest of the base packages 
    pacman -Su

    Now open up the Msys MinGW terminal
    To install gcc and g++ for C and C++ 
    For 64 bit
    pacman -S mingw-w64-x86_64-gcc
    For 32 bit
    pacman -S mingw-w64-i686-gcc

    To install the debugger ( gdb ) for C and C++
    For 64 bit
    pacman -S mingw-w64-x86_64-gdb
    For 32 bit
    pacman -S mingw-w64-i686-gdb

    To check
    gcc version : gcc --version
    g++ version : g++ --version
    gdb version : gdb --version

    After installing these programs, we need to set the Path environment variable.

([reference](https://www.youtube.com/watch?v=0HD0pqVtsmw))

### `Visual Code` 

Download: https://code.visualstudio.com

([reference](https://www.youtube.com/watch?v=JGsyJI8XG0Y))

Extensions: ...

([reference](https://www.youtube.com/watch?v=77v-Poud_io))

---
## New project

    #include <iostream>
    using namespace std;

    int main() {
        cout << "Hello World!" << endl;
        return 0;
    }

Download useful templates from [here](#).

- [[or here]()] Menu with color highlights
- [[or here]()] System summary file generator
- [[or here]()] Class hierarchy with inheritance

---
## Usage

### `Data Types`

    int number = 5;
    double numberWithDot = 1.75;
    short oneBitNumber = 127;
    positive short oneBitNumber = 255;
    long fourBitsNumber = 1234567890;
    long long eightBitsNumber = 12345678901234567890;

    string word = "Hello World!";
    char letter = 'H';

    boolean condition = true;

### `In / Out`

    cout << word << "Nice to meet you!"" << endl; 
    cin >> word;

>ex-C-eption

    printf()

### `If / Else`

    if (condition && true || false) {

    } else if () {

    }

### `Loops`

    for (int i = 0; i < 10; i++) {
        // Go to the next iteration.
        continue;
        // Leave the loop.
        break;
    }

    while (true) {
        break;
    }

    int counter = 0
    do {
        counter++
    } while (counter != 10)

## Libraries

### `CSTDIO`

    #import <cstdio>

    someFunction()