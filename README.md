# HelloWorld

Here you are all languages I've ever experienced. \
Believe me, it's better than [stackoverflow](https://stackoverflow.com/).

Dates and progression bars relate to documentation only. \
I learn programming since `2016`.

---
## Language

[C / C++](cpp/documentation.md) \
Started: **01.02.2023** \
Progress: [`##--------`]

[Bash](sh/documentation.md) \
Started: **not yet** \
Progress: [`----------`]

[Groovy](groovy/documentation.md) \
Started: **not yet** \
Progress: [`----------`]

[Python](py/documentation.md) \
Started: **not yet** \
Progress: [`----------`]

---
Planned also: C#, Java, PHP, HTML / CCS, JavaScript, Latex